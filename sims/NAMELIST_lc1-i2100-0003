&parallel_nml
 nproma         = 8  ! optimal setting 8 for CRAY; use 16 or 24 for IBM
 p_test_run     = .false.
 l_test_openmp  = .false.
 l_log_checks   = .false.
 num_io_procs   = 1   ! asynchronous output for values >= 1
 itype_comm     = 1
 iorder_sendrecv = 3  ! best value for CRAY (slightly faster than option 1)
/
&grid_nml
 dynamics_grid_filename  = "grid_DOM01.nc"
 radiation_grid_filename = ""
 dynamics_parent_grid_id = 0
 lredgrid_phys           = .false.
/
&initicon_nml
 ifs2icon_filename = "ifs2icon_init.nc"
 init_mode   = 2           ! operation mode 2: IFS
 zpbl1       = 500. 
 zpbl2       = 1000. 
 l_sst_in    = .true.		 ! Jennifer
/
&run_nml
 num_lev        = 90           ! 60
 dtime          = 180     ! timestep in seconds
 ldynamics      = .TRUE.       ! dynamics
 ltransport     = .true.
 iforcing       = 3            ! NWP forcing
 ltestcase      = .false.      ! false: run with real data
 msg_level      = 7            ! print maximum wind speeds every 5 time steps
 ltimer         = .false.      ! set .TRUE. for timer output
 timers_level   = 1            ! can be increased up to 10 for detailed timer output
 output         = "nml"
/
&nwp_phy_nml
 inwp_gscp       = 1
 inwp_convection = 1
 inwp_radiation  = 1
 inwp_cldcover   = 1
 inwp_turb       = 1
 inwp_satad      = 1
 inwp_sso        = 1
 inwp_gwd        = 1
 inwp_surface    = 1
 icapdcycl       = 3 ! apply CAPE modification to improve diurnalcycle over tropical land (optimizes NWP scores)
 latm_above_top  = .false.  ! the second entry refers to the nested domain (if present)
 efdt_min_raylfric = 7200.
 ldetrain_conv_prec = .false. ! ** new for v2.0.15 **; set .true. to activate detrainment of rain and snow; should be used in R3B08 EU-nest only (not for R2B07)!
 itype_z0         = 2
 icpl_aero_conv   = 0
 icpl_aero_gscp   = 0
! resolution-dependent settings - please choose the appropriate one
 dt_rad    = 2160.
 dt_conv   = 720.
 dt_sso    = 1440.
 dt_gwd    = 1440.
 lcloudradoff = .true.
! lcloudradonly = .true.  !! only heat with cre
 ldiag_ddt_temp_dyn2 = .true.
! lradoff = .true.        !! uncomment in case of no-radiation simulation
/
&turbdiff_nml
 tkhmin  = 0.75  ! new default since rev. 16527
 tkmmin  = 0.75  !           " 
 pat_len = 100.
 c_diff  = 0.2
 rat_sea = 10.  ! ** new since r20191: 8.5 for R3B7, 8.0 for R2B6 in order to get similar temperature biases in the tropics **
 ltkesso = .true.
 frcsmot = 0.2      ! these 2 switches together apply vertical smoothing of the TKE source terms
 imode_frcsmot = 2  ! in the tropics (only), which reduces the moist bias in the tropical lower troposphere
 ! use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects:
 itype_sher = 3    
 ltkeshs    = .true.
 a_hshr     = 2.0
/
&lnd_nml
 ntiles         = 3      !!! 1 for assimilation cycle and forecast
 nlev_snow      = 3      !!! 1 for assimilation cycle and forecast
 lmulti_snow    = .true. !!! .false. for assimilation cycle and forecast
 itype_heatcond = 2
 idiag_snowfrac = 2
 lsnowtile      = .false.  !! later on .true. if GRIB encoding issues are solved
 lseaice        = .true. !!Sophia Schäfer, 23/03/2017: reads in sea ice (e.g. zero ice), instead of setting by temperature
 llake          = .false.
 itype_lndtbl   = 3  ! minimizes moist/cold bias in lower tropical troposphere
 itype_root     = 2
/
&radiation_nml
 irad_o3       = 4 ! APE ozone
 irad_aero     = 0
 irad_cfc11    = 0
 irad_cfc12    = 0
 albedo_type   = 2 ! Modis albedo
 vmr_co2       = 348.0e-6
 vmr_ch4       = 1650.0e-09
 vmr_n2o       = 396.0e-09
 vmr_o2        = 0.20946
 izenith       = 3 ! perpetual equinox with diurnal cycle
/
&nonhydrostatic_nml
 iadv_rhotheta  = 2
 ivctype        = 2
 itime_scheme   = 4
 exner_expol    = 0.333
 vwind_offctr   = 0.2
 damp_height    = 50000.
 rayleigh_coeff = 0.10
 lhdiff_rcf     = .true.
 divdamp_order  = 24    ! for data assimilation runs, '2' provides extra-strong filtering of gravity waves 
 divdamp_type   = 32    !!! optional: 2 for assimilation cycle if very strong gravity-wave filtering is desired
 divdamp_fac    = 0.004
 l_open_ubc     = .false.
 igradp_method  = 3
 l_zdiffu_t     = .true.
 thslp_zdiffu   = 0.02
 thhgtd_zdiffu  = 125.
 htop_moist_proc= 22500.
 hbot_qvsubstep = 22500. ! use 19000. with R3B7
/
&sleve_nml
 min_lay_thckn   = 20.
 max_lay_thckn   = 400.   ! maximum layer thickness below htop_thcknlimit
 htop_thcknlimit = 14000. ! this implies that the upcoming COSMO-EU nest will have 60 levels
 top_height      = 75000.
 stretch_fac     = 0.9
 decay_scale_1   = 4000.
 decay_scale_2   = 2500.
 decay_exp       = 1.2
 flat_height     = 16000.
/
&dynamics_nml
 iequations     = 3
 idiv_method    = 1
 divavg_cntrwgt = 0.50
 lcoriolis      = .TRUE.
/
&transport_nml
 ctracer_list  = '12345'
 ivadv_tracer  = 3,3,3,3,3
 itype_hlimit  = 3,4,4,4,4
 ihadv_tracer  = 52,2,2,2,2
 iadv_tke      = 0
/
&diffusion_nml
 hdiff_order      = 5
 itype_vn_diffu   = 1
 itype_t_diffu    = 2
 hdiff_efdt_ratio = 24.0
 hdiff_smag_fac   = 0.025
 lhdiff_vn        = .TRUE.
 lhdiff_temp      = .TRUE.
/
&interpol_nml
 nudge_zone_width  = 8
 lsq_high_ord      = 3
 l_intp_c2l        = .true.
 l_mono_c2l        = .true.
 support_baryctr_intp = .false. !.true.
/
&extpar_nml
 extpar_filename = 'extpar_DOM01.nc'
 itopo          = 1
 n_iter_smooth_topo = 1
&io_nml
 itype_pres_msl = 4  ! IFS method with bug fix for self-consistency between SLP and geopotential
 itype_rh       = 1  ! RH w.r.t. water
/
&output_nml
 filetype                     =  5            ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  -1                     ! write all domains
 output_bounds                = 0., 10000000., 21600.   ! start, end, increment
 steps_per_file               = 4 
 include_last                 =  .TRUE.
 output_filename              = 'icon-lc1-i2100-0003-atmfields_ll' ! file name base
 ml_varlist                   = 'temp', 'u', 'v', 'w', 'pres', 'omega', 'pv', 'tot_qv_dia', 'tot_qi_dia', 'tot_qc_dia', 'clc', 'rho', 'z_ifc','pres','qv' ! needed for deriving heating rates on model levels using fluxes
! pl_varlist                   = 'temp', 'u', 'v', 'w', 'omega', 'pv', 'tot_qv_dia', 'tot_qi_dia', 'tot_qc_dia', 'clc'
 remap                        = 1
! p_levels     = 1000, 2000, 3000, 5000, 7000, 10000, 15000, 20000, 25000, 30000, 40000, 50000, 60000, 70000, 85000, 92500, 100000
 reg_lon_def                  = -180.,1,179.5
 reg_lat_def                  = 89.5,-1, -89.5
/
&output_nml
 filetype                     =  5            ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  -1                     ! write all domains
 output_bounds                = 0., 10000000., 21600.   ! start, end, increment
 steps_per_file               = 4 
 include_last                 =  .TRUE.
 output_filename              = 'icon-lc1-i2100-0003-radfields_ll' ! file name base
 ml_varlist                   = 'lwflxall', 'lwflxclr','swflxclr','swflxall'
 remap                        = 1
 reg_lon_def                  = -180.,1,179.5
 reg_lat_def                  = 89.5,-1, -89.5
/
&output_nml
 filetype                     =  5            ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  -1                     ! write all domains
 output_bounds                = 0., 10000000., 21600.   ! start, end, increment
 steps_per_file               = 4
 include_last                 =  .TRUE.
 output_filename              = 'icon-lc1-i2100-0003-phys_tendencies_ll' ! file name base
 ml_varlist                   = 'ddt_temp_dyn2','ddt_temp_radsw','ddt_temp_radlw','ddt_temp_turb','ddt_temp_pconv','ddt_temp_gscp','ddt_temp_mphy','ddt_temp_totnwpphy','pres'    ! for no-radiation or cloud rad only change radlw/radsw to radlwnw/radswnw, for clear-sky delete cs 
 remap                        = 1
 reg_lon_def                  = -180.,1,179.5
 reg_lat_def                  = 89.5,-1, -89.5
/
&output_nml
 filetype                     =  5              ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  -1                       ! write all domains
 output_bounds                =  0., 10000000., 21600.    ! start, end, increment
 steps_per_file               =  4
 include_last                 = .TRUE.
 output_filename              = 'icon-lc1-i2100-0003-sfcfields_ll'   ! file name base
 ml_varlist                   = 'pres_sfc','group:precip_vars','tqv','tqc','tqi','shfl_s','lhfl_s','t_g','t_2m','qv_2m','rh_2m','u_10m','v_10m','clct','clcl','clcm','clch', 'sod_t','sou_t','thb_t','sou_s','sob_s','thu_s','thb_s','lwtoaclr','swtoaclr','lwsfcclr','swsfcclr'
 remap                        = 1
 reg_lon_def                  = -180.,1,179.5
 reg_lat_def                  = 89.5,-1, -89.5
/
