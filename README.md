# Code repository for publication "Tug-of-war on idealized midlatitude cyclones between radiative heating from low-level and high-level clouds"

The paper is published in the AGU journal Geophysical Research Letters.

The paper is based on the MSc thesis of Klara Butz, 2022, The radiative impact of clouds on idealized extratropical cyclones, https://ubdata.univie.ac.at/AC16593523.

**Author:** Aiko Voigt, IMG, University of Vienna, aiko.voigt@univie.ac.at.

**The repository contains:**

* sims: ICON simulation logfiles
* literature-search: excel sheet for Web of Science search of previous studies of baroclinic life cyles
* python3: python3 scripts and notebooks, with the following subdirectories:

   - plots4paper: jupyter notebooks for figures used in GRL paper, also figure pdfs postprocessed with inkscape and pdfcrop; mostly one jupyter notebook per figure, but some of the supplmementary figures are done in a joint notebook
   - environment: yml file that documents the python environment used at DKRZ jupyterhub server
   - helperfuncs: python functions to faciliate loading of data and analysis, e.g., domain means, cyclone metrics, averaging of the 6 cyclones into a mean cyclone etc.
   
To load the simulations, use the dictionary dict_expid defined in myfunctions.py, which links the simulation id to the simulation setup. For an illustration of how to use the dictionary, see for example figure_5.ipynb.

**Simulation summary:** (also found in python3/helperfuncs/myfunctions.py)

*ICON2.1:*
* i2100-0001: ICON2.1 ALLSKYRAD (all-sky radiative heating)
* i2100-0002: ICON2.1 NORAD (no radiative heating)
* i2100-0003: ICON2.1 CLRSKYRAD (clear-sky radiative heating)
* i2100-0004: ICON2.1 CRHONLY (cloud-radiative heating following the Keshtgar et al., 2023 method)
* i2100-0005: ICON2.1 PBLCRHONLY (cloud-radiative heating below 2 km)
* i2100-0007: ICON2.1 CRHONLY (same as i2100-0004 except for the output)
* i2100-0008: ICON2.1 FTCRHONLY (cloud-radiative heating above 2km)

*ICON2.6:*
* i2622-v2-0001: ICON2.6 ALLSKYRAD
* i2622-v2-0003: ICON2.6 NORAD
* i2622-v2-0004: ICON2.6 CLRSKYRAD
* i2622-v2-0005: ICON2.6 CRHONLY
* i2622-v2-0006: ICON2.6 PBLCRHONLY
* i2622-v2-0007: ICON2.6 FTCRHONLY


