# a set of functions to analyse cyclone characteristics and for plotting
# author: Aiko Voigt, UNIVIE

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt


# dictionary of expids and their meaning
dict_expid = dict([
    ("i2100-0001", "ICON2.1 ALLSKYRAD"),
    ("i2100-0002", "ICON2.1 NORAD"),
    ("i2100-0003", "ICON2.1 CLRSKYRAD"),
    ("i2100-0004", "ICON2.1 CRHONLY"),
    ("i2100-0005", "ICON2.1 PBLCRHONLY"),
    ("i2100-0007", "ICON2.1 CRHONLY"), # same as 0004 except for the output
    ("i2100-0008", "ICON2.1 FTCRHONLY"),
    ("i2622-v2-0001", "ICON2.6 ALLSKYRAD"),
    ("i2622-v2-0003", "ICON2.6 NORAD"),
    ("i2622-v2-0004", "ICON2.6 CLRSKYRAD"),
    ("i2622-v2-0005", "ICON2.6 CRHONLY"),
    ("i2622-v2-0006", "ICON2.6 PBLCRHONLY"),
    ("i2622-v2-0007", "ICON2.6 FTCRHONLY")
])


def domainmean(data):
    """Computes domain mean for a single level, not taking into account the density of air at that level.
    Therefore, the function is appropriate for fields at the Earth's surface or at the top of the atmosphere.
    
    The computation is done over entire domain. For a global dataset, this means that 
    a global mean will be computed. For a regional dataset, the regional domain will
    be computed."
    """
    if "lon" in data.dims: data = data.mean("lon")
    weights = np.cos(np.deg2rad(data.lat))
    weights.name = "weights"
    data_weighted = data.weighted(weights)
    return data_weighted.mean("lat")


def domainmean_per_level(data, rho):
    """Computes domain mean for a single level, taking into account the density of air at that level.
    Therefore, the function is appropriate for fields at model levels, e.g., atmospheric temperature.
    
    The function is the counterpart to the function "domainmean".
    
    The computation is done over entire domain. For a global dataset, this means that 
    a global mean will be computed. For a regional dataset, the regional domain will
    be computed."
    """
    data.load(); rho.load()
    weights = np.cos(np.deg2rad(data.lat)) * rho
    weights.name = "weights"
    data_weighted = data.weighted(weights)
    if "lon" in data.dims: 
        mean = data_weighted.mean(["lat", "lon"])
    else:
        mean = data_weighted.mean("lat")
    return mean


def make_meancyclone(da):
    """Constructs a mean cyclone from the 6 simulated cyclones for data array.
    
    This is done by averaging over the 6 longitudes that are separated by dlon=60 deg."""
    # make lon the last dimension
    _da = da.transpose(...,"lon")
    _shape = _da.shape # this is (something, lon)
    _nlon = _shape[-1]  
    _mean = _da.values.reshape(_shape[:-1]+(6,int(_nlon/6))).mean(axis=len(_shape)-1)
    # return as data array
    _lon0 = _da["lon"][0] # to make lon start at 0
    return xr.DataArray( _mean, dims = _da.dims, 
                         coords= {**_da.drop("lon").coords, **{"lon":-_lon0+_da.coords["lon"][0:int(_nlon/6)]}} )


def eke(data, meancyclone=False):
    """Computes eddy kinetic energy,
    
    Eddies are defined as the deviation from the zonal mean at a given timestep."""
    u = data["u"]
    v = data["v"]
    if meancyclone:
        u = make_meancyclone(u)
        v = make_meancyclone(v)
    aux = 0.5*( np.power(u-u.mean("lon"),2) + np.power(v-v.mean("lon"),2))
    return aux.mean("lon")   


def corepressure(data, meancyclone=False):
    """Computes cyclone core pressure as the global minimum of variable pres_sfc.
    
    Default is to consider the surface pressure of the mean cyclone, if this is not desired the 
    optional argument meancyclone needs to be set to False."""
    pres_sfc = data["pres_sfc"]
    if meancyclone: pres_sfc = make_meancyclone(data["pres_sfc"])
    return pres_sfc.min(("lat","lon"))


def zlevels():
    """Return height of z-levels in km above ground
    
    As z-levels are the same for all simulations, we can just get them from one simulation."""
    path = "/work/bb1135/b380459/Butz-MSc2022/consolidated"
    z_ifc = xr.open_dataset(path+"/icon-lc1-i2100-0002-atmfields_ll_DOM01_ML.nc")["z_ifc"].squeeze().isel(time=0, lat=0, lon=0).values
    z = 0.5*(z_ifc[1:91]+z_ifc[0:90]) * 1e-3
    return z

def calculate_crh(ds, expid):
    """Calculates cloud-radiative heating within the atmosphere.
    
    The calculation of CRH depends on the simulation, hence the expid is necessary. crhlw is the longwave part of the heating."""
    if expid=="i2100-0001":
        ds["crhlw"] = 86400*(ds["ddt_temp_radlw"]-ds["ddt_temp_radlwcs"])
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]-ds["ddt_temp_radlwcs"]+ds["ddt_temp_radsw"]-ds["ddt_temp_radswcs"])
    elif expid=="i2100-0002":
        ds["crhlw"] = 86400*(ds["ddt_temp_radlwnw"]-ds["ddt_temp_radlwcs"])
        ds["crh"] = 86400*(ds["ddt_temp_radlwnw"]-ds["ddt_temp_radlwcs"]+ds["ddt_temp_radswnw"]-ds["ddt_temp_radswcs"])
    elif expid=="i2100-0003":
        ds["crhlw"] = 0*ds["ddt_temp_radlw"]
        ds["crh"] = 0*ds["ddt_temp_radlw"]
    elif expid=="i2100-0004":
        ds["crhlw"] = 86400*(ds["ddt_temp_radlwnw"]-ds["ddt_temp_radlwcs"])
        ds["crh"] = 86400*(ds["ddt_temp_radlwnw"]-ds["ddt_temp_radlwcs"]+ds["ddt_temp_radswnw"]-ds["ddt_temp_radswcs"])
    elif expid=="i2100-0005":
        ds["crhlw"] = 86400*ds["ddt_temp_radlw"]
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]+ds["ddt_temp_radsw"])
    elif expid=="i2100-0007":
        ds["crhlw"] = 86400*ds["ddt_temp_radlw"]
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]+ds["ddt_temp_radsw"])
    elif expid=="i2100-0008":
        ds["crhlw"] = 86400*ds["ddt_temp_radlw"]
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]+ds["ddt_temp_radsw"])
    elif expid=="i2622-v2-0001":
        ds["crhlw"] = 86400*(ds["ddt_temp_radlw"]-ds["ddt_temp_radlwcs"])
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]-ds["ddt_temp_radlwcs"]+ds["ddt_temp_radsw"]-ds["ddt_temp_radswcs"])
    elif expid=="i2622-v2-0003":
        ds["crhlw"] = 86400*(ds["ddt_temp_radlwnw"]-ds["ddt_temp_radlwcs"])
        ds["crh"] = 86400*(ds["ddt_temp_radlwnw"]-ds["ddt_temp_radlwcs"]+ds["ddt_temp_radswnw"]-ds["ddt_temp_radswcs"])
    elif expid=="i2622-v2-0004":
        ds["crhlw"] = 0*ds["ddt_temp_radlw"]
        ds["crh"] = 0*ds["ddt_temp_radlw"]
    elif expid=="i2622-v2-0005":
        ds["crhlw"] = 86400*ds["ddt_temp_radlw"]
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]+ds["ddt_temp_radsw"])        
    elif expid=="i2622-v2-0006":
        ds["crhlw"] = 86400*ds["ddt_temp_radlw"]
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]+ds["ddt_temp_radsw"]) 
    elif expid=="i2622-v2-0007":
        ds["crhlw"] = 86400*ds["ddt_temp_radlw"]
        ds["crh"] = 86400*(ds["ddt_temp_radlw"]+ds["ddt_temp_radsw"]) 
    else:
        print("ERROR: unknown expid in recalculate_crh")
    return ds


def calculate_latentheating(ds):
    """Calculates latent heating in K/day"""
    ds["latheat"] = 86400*ds["ddt_temp_mphy"]
    return ds


def calculate_moistheating(ds):
    """Calculates heating related to moist processes of microphysics, saturation adjustment and moist convection in K/day"""
    ds["moistheat"] = 86400*(ds["ddt_temp_mphy"]+ds["ddt_temp_pconv"])
    return ds


def load_data_cyclonemetrics(expid: str, chunks=None):
    """Loads data for simulation "expid" to compute cyclone metrics of EKE at 300hPa and 925hPa and core pressure.
    
    Returns an xarray dataset.
    
    "chunks" can be set to use lazy loading with dask. If None, dask is nit used.
    "expid" is added as an attribute to the dataset.
    "setup" is an added as an attribute based on expid and dict_expid."""
    path = "/work/bb1135/b380459/Butz-MSc2022/consolidated"
    sfc  = xr.open_dataset(path+"/icon-lc1-"+expid+"-sfcfields_ll_DOM01_ML.nc", chunks=chunks).squeeze()
    atm  = xr.open_dataset(path+"/icon-lc1-"+expid+"-atmfields_ll_DOM01_PL.nc", chunks=chunks).rename({"plev_3": "lev"}).squeeze()
    ds   = xr.merge([sfc,atm]).squeeze()
    del sfc, atm
    # reorder latitude if not from NP to SP
    if ds.lat[0]<ds.lat[1]: 
        ds = ds.reindex(lat=list(reversed(ds.lat)))
    # add expid and setup as global attributes
    ds.attrs["expid"]=expid
    ds.attrs["setup"]=dict_expid[expid]
    return ds


def load_data(expid: str, chunks=None):
    """Loads icon model output for the simulation "expid" as an xarray dataset.
    
    "chunks" can be set to use lazy loading with dask. If None, dask is not used.
    "expid" is added as an attribute to the dataset.
    "setup" is an added as an attribute based on expid and dict_expid."""
    path = "/work/bb1135/b380459/Butz-MSc2022/consolidated"
    sfc  = xr.open_dataset(path+"/icon-lc1-"+expid+"-sfcfields_ll_DOM01_ML.nc", chunks=chunks).squeeze()
    atm  = xr.open_dataset(path+"/icon-lc1-"+expid+"-atmfields_ll_DOM01_ML.nc", chunks=chunks).squeeze()
    dTdt = xr.open_dataset(path+"/icon-lc1-"+expid+"-phys_tendencies_ll_DOM01_ML.nc", chunks=chunks).squeeze()
    ds   = xr.merge([sfc,atm,dTdt]).squeeze()
    del sfc, atm, dTdt
    # reorder latitude if not from NP to SP
    if ds.lat[0]<ds.lat[1]: 
        ds = ds.reindex(lat=list(reversed(ds.lat)))
    # add cloud-radiative heating, latent heating and heating from moist processes
    ds = calculate_crh(ds, expid=expid)
    ds = calculate_latentheating(ds)
    ds = calculate_moistheating(ds)
    # vertical velocity at 1, 2, 5, 8 and 10 km height
    z = zlevels()
    def add_omega_at_km(height):
        iheight = np.argmin(np.abs(z-height))
        ds["w"+str(height)+"km"] = ds["omega"].isel(height=iheight)
        ds["w"+str(height)+"km"] = ds["omega"].isel(height=iheight)
    for height in [1,2,5,8,10]:
        add_omega_at_km(height)
    # add expid and setup as global attributes
    ds.attrs["expid"]=expid
    ds.attrs["setup"]=dict_expid[expid]
    return ds


def beautify_timeseries(ax):
    """ Makes plots of time series  nicer in terms of axes and labeling"""
    # adjust spines
    ax = plt.gca()
    ax.spines['top'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['bottom'].set_position(('data',0))
    plt.xlim(0,11)
    plt.xlabel("day", loc="right", size=12)
    plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11],["", "1","","3","","5","","7","","9","","11"], size=10);